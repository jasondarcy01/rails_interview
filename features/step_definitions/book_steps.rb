Given(/^the following books records$/) do |table|
  table.hashes.each do |hash|
    Book.create! :title => hash["title"], :author => hash["author"], :isbn10 => hash["isbn10"], :isbn13 => hash["isbn13"], :rank => hash["rank"]
  end
end


When(/^I go to the list of books$/) do
  visit books_by_popularity_index_path
end


Then(/^I should see the books in this order:$/) do |table|
  expected_order = table.raw
  actual_order = page.all(:css, '.title').collect(&:text)
  actual_order.should == expected_order.flatten
end

