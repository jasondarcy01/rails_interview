class Book < ActiveRecord::Base
  attr_accessible :title, :body, :author, :isbn10, :isbn13, :rank
   validates :title, presence: true
   validates :author, presence: true
   validates :rank, presence: true
   validates :isbn10, length: { is: 10 }
   validates :isbn10, presence: true
   validates :isbn10, uniqueness: true
   validates :isbn10, numericality: true
   validates :isbn13, length: { is: 13 }
   validates :isbn13, presence: true
   validates :isbn13, numericality: true
   validates :isbn13, uniqueness: true
end
