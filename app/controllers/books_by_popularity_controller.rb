class BooksByPopularityController < ApplicationController
  def index
    @books = Book.order("books.rank ASC").find(:all, :order => 'title')
  end
end
